"""empty message

Revision ID: 380a3810f3d6
Revises: 
Create Date: 2018-12-22 00:29:05.069536

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '380a3810f3d6'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('movies',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('user', sa.String(length=50), nullable=True),
    sa.Column('title', sa.String(length=50), nullable=True),
    sa.Column('description', sa.String(length=500), nullable=True),
    sa.Column('date', sa.Date(), nullable=True),
    sa.Column('rating', sa.Integer(), nullable=True),
    sa.Column('watched', sa.Boolean(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('number',
    sa.Column('numberino', sa.String(length=50), nullable=False),
    sa.Column('name', sa.String(length=50), nullable=True),
    sa.PrimaryKeyConstraint('numberino')
    )
    op.create_table('users',
    sa.Column('username', sa.String(length=50), nullable=False),
    sa.Column('password', sa.String(length=50), nullable=True),
    sa.PrimaryKeyConstraint('username')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('users')
    op.drop_table('number')
    op.drop_table('movies')
    # ### end Alembic commands ###
