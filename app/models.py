from app import db

class number(db.Model):
    numberino = db.Column(db.String(50), primary_key=True)
    name = db.Column(db.String(50))
    '''id = db.Column(db.Integer, primary_key=True)
    address = db.Column(db.String(500), index=True, unique=True)
    start_date = db.Column(db.DateTime)
    duration = db.Column(db.Integer)
    rent = db.Column(db.Float)
'''
    def __init__(self, numberino, name):
        self.numberino = numberino
        self.name = name

    def __repr__(self):
        return 'name {}'.format(self.name)

class movies(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user = db.Column(db.String(50))
    title = db.Column(db.String(50))
    description = db.Column(db.String(500))
    date = db.Column(db.Date)
    rating = db.Column(db.Integer)
    watched = db.Column(db.Boolean)

class users(db.Model):
    username = db.Column(db.String(50), primary_key=True)
    password = db.Column(db.String(50))

    def __init__(self,username,password):
        self.username=username
        self.password=password
